import { EventEmitter, Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService {
   recipeSelected = new EventEmitter<Recipe>();


   private recipes:Recipe[] = [
        new Recipe('test1',
        'this is test1',
        'https://i2.wp.com/hiii.design/wp-content/uploads/2019/04/cropped-Logo-Hiii-2.jpg?fit=512%2C512&ssl=1',
        [
           new Ingredient('Test',1),
           new Ingredient('Test2',5)
        ]
        ),
        
        new Recipe('test2',
        'this is test2',
        'https://i2.wp.com/hiii.design/wp-content/uploads/2019/04/cropped-Logo-Hiii-2.jpg?fit=512%2C512&ssl=1',
        [
         new Ingredient('Test3',7),
         new Ingredient('Test4',10)
         ]
        )
      ];

   constructor(private shopService:ShoppingListService){

   }
      getRecipes(){
         return this.recipes.slice();
      }
      getRecipe(index: number){ return this.recipes[index];}

      addSelectIng(ingre:Ingredient[]){
         this.shopService.addIngredientss(ingre);
      }
}