import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id:number;
  editMode = false;

  constructor(private route:ActivatedRoute ) { }

  ngOnInit() {
    this.route.params
        .subscribe(
          (params:Params) => {
            this.id = +params['id']; //+ is to convert the string to a number
            this.editMode = params['id'] != null; // to check if its true or false ... this returns a boolen value
            //console.log(this.editMode);
          }
        );
  }

}
