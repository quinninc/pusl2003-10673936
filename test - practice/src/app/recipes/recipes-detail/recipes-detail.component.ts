import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipes-detail',
  templateUrl: './recipes-detail.component.html',
  styleUrls: ['./recipes-detail.component.css']
})
export class RecipesDetailComponent implements OnInit {
recipe:Recipe;
id:number;
  constructor(private recipeService:RecipeService,
              private route:ActivatedRoute,
              private currentRoute:Router) { }

  ngOnInit() {
    //const id = this.route.snapshot.params['id'];
    this.route.params
        .subscribe(
          (paramssssss:Params) => { this.id = +paramssssss['id'];
                                   this.recipe = this.recipeService.getRecipe(this.id); }
        );
  }

  onSelectIng(){
    this.recipeService.addSelectIng(this.recipe.ingredients);
  }

  onEditRecipe(){
     // this.currentRoute.navigate(['edit'] , {relativeTo:this.route});  this also works but not the best approch
     this.currentRoute.navigate(['../',this.id,'edit'], {relativeTo:this.route});
  }

}
 