import { Component, OnInit } from '@angular/core';
import { Recipe } from './recipe.model';
import { RecipeService } from './recipe.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css'],
  providers:[RecipeService]
})
export class RecipesComponent implements OnInit {
  selectedRecipe:Recipe;
  constructor(private resService:RecipeService) { }

  ngOnInit() {
    this.resService.recipeSelected
            .subscribe(
              (recipe333:Recipe) =>{ this.selectedRecipe = recipe333; }
            );

  }

}
