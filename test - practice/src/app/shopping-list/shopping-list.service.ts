import { Ingredient } from '../shared/ingredient.model';
import { EventEmitter } from '@angular/core';

export class ShoppingListService{
    ingredientChanged = new EventEmitter<Ingredient[]>()
   private ingredients:Ingredient[] = [
        new Ingredient('apples',5),
        new Ingredient('Oranges',10)
      ];

      getIngredients(){return this.ingredients.slice();}

      addIngredient(ing:Ingredient)
      {this.ingredients.push(ing);
       this.ingredientChanged.emit(this.ingredients.slice()); }

       addIngredientss(ing:Ingredient[]){
        //for(let ingre of ing){
        //  this.addIngredient(ingre);
      //  }  
          this.ingredients.push(...ing)
          this.ingredientChanged.emit(this.ingredients.slice());
       }
}