import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Post } from './post.model';
import { postService } from './post.service';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit ,OnDestroy {
  loadedPosts:Post[] = [];
  @ViewChild('postForm') signupform:NgForm;
  isFetching = false;
  error=null;
  private errorSub: Subscription;
  constructor(private http: HttpClient, private pService:postService) {}

  ngOnInit() {
   this.errorSub = this.pService.error.subscribe(errorMessage =>{
      this.error = errorMessage;
    });

    this.isFetching=true;
    this.pService.onFetchPosts().subscribe(post =>{
      this.loadedPosts=post;
      this.isFetching=false;
    },
    errorrrr =>{
        this.error = errorrrr.error.error;
        console.log(errorrrr);
    });
  }
  onCreatePost(postData:{title:string; content:string}){
  //onCreatePost(postData: Post) {
    // Send Http request
    console.log(postData);
    this.pService.onCreateAndStorePosts(postData);
    this.signupform.reset();
  }

  onFetchPosts() {
    // Send Http request
    this.isFetching=true;
    this.pService.onFetchPosts().subscribe(posts =>{
      this.loadedPosts=posts;
      this.isFetching=false;
      
      

    },
    errorrrr =>{
        this.error = errorrrr.message;
        console.log("byee");
    }
    );
  }

  onClearPosts() {
    // Send Http request
    this.pService.deletePosts().subscribe(() =>{
      this.loadedPosts = [];
    });
  }

//private fetchposts(){
// this.isFetching =true;
//}

ngOnDestroy(){
this.errorSub.unsubscribe();
}
}

