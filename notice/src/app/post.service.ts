import { Injectable } from '@angular/core';
import { Post } from './post.model';
import { HttpClient } from '@angular/common/http';
import { map,catchError } from 'rxjs/operators'
import { Subject,throwError } from 'rxjs';

@Injectable({providedIn:'root'})
export class postService{
  error = new Subject<string>();



    constructor(private http:HttpClient){}

onCreateAndStorePosts(postData:Post){
    this.http
    .post<{name:string}>(
      'https://web-test-6615b.firebaseio.com/posts.json',//this 'post' data is sort of a folder in the real time db
      postData //the parameters of the function 
    )
    .subscribe(responseDataaa => {
      console.log(responseDataaa);
    },errorh =>{
      this.error.next(errorh.message);
    }
    );
}



  

onFetchPosts(){
  return this.http.get<{[key:string]:Post}>('https://web-test-6615b.firebaseio.com/posts.json')
  .pipe(map(responseData => {
      const postArray:Post[] = [];
      for(const hii in responseData){
        if(responseData.hasOwnProperty(hii)){
        postArray.push({...responseData[hii] ,id:hii});
        }
      }
      return postArray;
    }),
    catchError(errorRes =>{
      //send to analytics server
      console.log("hiii");
      return throwError(errorRes);
  })    
  );
  
}

deletePosts(){
 return this.http.delete('https://web-test-6615b.firebaseio.com/posts.json')
}

}