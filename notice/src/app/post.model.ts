export interface Post {
    title:string;
    content:String;
    id?:string;
}