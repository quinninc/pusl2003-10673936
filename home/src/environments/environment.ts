// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyCuSR0oq6Yoa_hmcuerFa6FPlQPEzFnSDI",
    authDomain: "web-test-6615b.firebaseapp.com",
    databaseURL: "https://web-test-6615b.firebaseio.com",
    projectId: "web-test-6615b",
    storageBucket: "web-test-6615b.appspot.com",
    messagingSenderId: "295490819449",
    appId: "1:295490819449:web:f96f92a0d05b5503838ba8",
    measurementId: "G-8W7XL8CBJ3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
