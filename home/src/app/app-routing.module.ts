import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { TabsComponent } from './tabs/tabs.component';
import { CalanderComponent } from './tabs/calander/calander.component';


const routes: Routes = [
  { path:'login', component:LoginComponent
},
{ path:'tabs', component:TabsComponent },
{ path:'', redirectTo: '/login',pathMatch:'full' },
{ path: 'id' ,component:CalanderComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
