import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  newUSer:any;
  constructor(private afAuth:AngularFireAuth,
              private db:AngularFirestore,
              private router:Router) { }

    createUser(user){
      this.afAuth.auth.createUserWithEmailAndPassword(user.email,user.password).
      then(userCredentials =>{
        this.newUSer = user;
        console.log(userCredentials);
        userCredentials.user.updateProfile({
          displayName: user.email
        });
        this.insertUSerData(userCredentials).then(()=>{
          this.router.navigate(['/tabs']);
        })
      });
    }

    insertUSerData(usercred:firebase.auth.UserCredential){
      return this.db.doc(`users/${usercred.user.uid}`).set({
        email:this.newUSer.email,
        role:'network user'

      })
    }
}
