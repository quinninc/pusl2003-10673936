import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLoginMode = true;
  authError:any;
  
  signupForm: FormGroup;

    constructor(private authS:AuthService) {}

    onSwitchMode(){
        this.isLoginMode = !this.isLoginMode
    }
    createUser(User){
      this.authS.createUser(User.value);
    }

    onSubmit(){
      
    }
  

  ngOnInit(): void {

    this.signupForm = new FormGroup({
      'userData' : new FormGroup({
        'username' : new FormControl(null,Validators.required),
        'email' : new FormControl(null , [Validators.required,Validators.email])
      }),
      'genderrr' : new FormControl('male'),
      'hobbies' : new FormArray([])
    });
  }

}
